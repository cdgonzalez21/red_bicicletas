var Bicicleta= require('../models/bicicleta');

exports.bicicleta_list=function(req,res){

    res.render('bicicletas/index',{bicis: Bicicleta.allBicis});    

}

exports.bicicleta_create_get=function(req,res){
            res.render('bicicletas/create');
}

exports.bicicleta_create_post=function(req,res){
    var bicic = new Bicicleta(req.body.id , req.body.color , req.body.modelo);
    bicic.ubicacion =[req.body.lat , req.body.lng];
    Bicicleta.add(bicla);
    res.redirect('/bicicletas');

}

exports.bicicleta_delete_post=function(req, res){
    Bicicleta.removeById(req.body.id);
    res.redirect('/bicicletas');
}

exports.bicicleta_update_get=function(req,res){
    var bici=Bicicleta.findById(req.params.id)
    res.render('bicicletas/update',{bici});
}

exports.bicicleta_update_post=function(req,res){
var bicic=Bicicleta.findById(req.params.id)
bicic.id=req.body.id;
bicic.modelo=req.body.modelo;
bicic.color=req.body.color;
bicic.ubicacion =[req.body.lat , req.body.lng];

res.redirect('/bicicletas');

}

exports.bicicleta_view_get=function(req,res){
    var bici=Bicicleta.findById(req.params.id)
    res.render('bicicletas/datos',{bici});
}
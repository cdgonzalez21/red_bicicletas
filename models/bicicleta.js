let Bicicleta = function(id,color,modelo,ubicacion){
    this.id=id;
    this.color=color;
    this.modelo=modelo;
    this.ubicacion=ubicacion
}

Bicicleta.prototype.toString=function(){
    return 'id: ' + this.id + " | color: "+this.color;
}

Bicicleta.allBicis=[];

Bicicleta.add =function(aBici){
    Bicicleta.allBicis.push(aBici);
};

let a = new Bicicleta(1,'Azul','Urbana',[4.747810, -74.046877]);
let b = new Bicicleta(2,'Dorado','Urbana',[4.751146, -74.046845]);
Bicicleta.add(a);
Bicicleta.add(b);
Bicicleta.findById=function(aBiciId){
   
    let aBici = Bicicleta.allBicis.find(x=>x.id==aBiciId);
    if(aBici)
        return aBici;
    else
        throw new Error(`El id ${aBici} no existe`);
}

Bicicleta.removeById=function(aBiciId){
    let aBici = Bicicleta.findById(aBiciId);
    for(let i=0;i<Bicicleta.allBicis.length;i++){
        if(Bicicleta.allBicis[i].id == aBiciId){
            Bicicleta.allBicis.splice(i,1);
            break;
        }
    }
}

module.exports = Bicicleta;

